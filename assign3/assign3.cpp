#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <time.h>
#include <sys/time.h>

int timeval_subtract(struct timeval* result,
                     struct timeval* t2,struct timeval* t1) {
    unsigned int resolution=1000000;
    long int diff = (t2->tv_usec + resolution * t2->tv_sec) -
                    (t1->tv_usec + resolution * t1->tv_sec) ;
    result->tv_sec = diff / resolution;
    result->tv_usec = diff % resolution;
    return (diff<0);
}

void matrix_operation(float *m_in, float *m_out, int n){
    for(int i = 0; i < n; i++){
        float accum = m_in[i*64]*m_in[i*64];
        m_out[i*64] = accum;
        for(int j = 1; j < 64; j++){
            float tmp_a = m_in[i*64+j];
            accum = sqrt(accum) + tmp_a*tmp_a;
            m_out[i*64+j] = accum;
        }
    }
}


void transpose_test(bool tilling = false){
    int rows = 20;
    int collums = 64;
    int memsize = sizeof(float)*rows*collums;
    float *m_in = (float*) malloc(memsize);
    float *m_out = (float*) malloc(memsize);
    float *d_in;
    float *d_out;

    unsigned long int elapsed;
    struct timeval t_start,t_end,t_diff;


    for(int i = 0; i < rows; i++){
        for(int j = 0; j < collums; j++){
            m_in[i*collums+j] = 1.0+j;
        }
    }    
    matrix_operation(m_in,m_out,rows);
    bool valid = true;
    for(int i = 0; i < rows; i++){
        for(int j = 0; j < collums; j++){
            std::cout << m_out[i*collums+j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << valid << "\n";

    timeval_subtract(&t_diff,&t_end,&t_start);
    elapsed=(t_diff.tv_sec*1e6 +t_diff.tv_usec);
    double flops = rows*collums;
    double gigaFlops=(flops*1.0e-3f) / elapsed;
    printf("Amount of GFPLOS %f \n",gigaFlops);
}

int main(int argc, char** argv) {

    transpose_test(false);
    transpose_test(true);
    return 0;
}


