#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <time.h>
#include <sys/time.h>
#include "transpose.cu.h"
#include "matrix_mul.cu.h"
#include "matrix_assign2.cu.h"

int timeval_subtract(struct timeval* result,
                     struct timeval* t2,struct timeval* t1) {
    unsigned int resolution=1000000;
    long int diff = (t2->tv_usec + resolution * t2->tv_sec) -
                    (t1->tv_usec + resolution * t1->tv_sec) ;
    result->tv_sec = diff / resolution;
    result->tv_usec = diff % resolution;
    return (diff<0);
}


void transpose_test(bool tilling){
    int rows = 10000;
    int collums = 12000;
    int const T = 32;
    int dimx = (collums+T-1)/T;
    int dimy = (rows+T-1)/T;
    dim3 block(T,T,1), grid(dimx,dimy,1);

    int memsize = sizeof(float)*rows*collums;
    float *m_in = (float*) malloc(memsize);
    float *m_out = (float*) malloc(memsize);
    float *d_in;
    float *d_out;

    unsigned long int elapsed;
    struct timeval t_start,t_end,t_diff;

    cudaMalloc((void**)&d_in , memsize);
    cudaMalloc((void**)&d_out, memsize);

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < collums; j++){
            m_in[i*collums+j] = 1.0+j;
        }
    }


    cudaMemcpy(d_in, m_in, memsize, cudaMemcpyHostToDevice);

    gettimeofday(&t_start, NULL);
    if(tilling){
        const int T = 32;
        tilling_transpose_kernel<T><<<grid,block>>>(d_in,d_out,rows,collums);
        cudaThreadSynchronize();
    } else {
        naive_transpose_kernel<<<grid,block>>>(d_in,d_out,rows,collums);
        cudaThreadSynchronize();
    }
    gettimeofday(&t_end, NULL);
    cudaMemcpy(m_out, d_out, memsize, cudaMemcpyDeviceToHost);

    bool valid = true;
    for(int i = 0; i < rows; i++){
        for(int j = 0; j < collums; j++){
            if(m_out[j*rows+i] != m_in[i*collums+j]){
                valid = false;
            }
        }
    }
    if(valid){
        std::cout << "Valid\n";
    }else{
        std::cout << "Invalid\n";
    }
    free(m_in);
    free(m_out);
    cudaFree(d_out);
    cudaFree(d_in);
    timeval_subtract(&t_diff,&t_end,&t_start);
    elapsed=(t_diff.tv_sec*1e6 +t_diff.tv_usec);
    double flops = rows*collums;
    double gigaFlops=(flops*1.0e-3f) / elapsed;
    printf("Amount of GFPLOS %f \n",gigaFlops);
}


void assign2_test(bool transposed){
    int rows = 10000;
    int collums = 64;
    int const T = 32;
    int dimy = (rows+T-1)/T;
    dim3 block(T,1,1), grid(dimy,1,1);

    int memsize = sizeof(float)*rows*collums;
    float *m_in = (float*) malloc(memsize);
    float *m_out = (float*) malloc(memsize);
    float *seq_m_out = (float*) malloc(memsize);
    float *d_in;
    float *d_out;
    float *d1_out;

    unsigned long int elapsed;
    struct timeval t_start,t_end,t_diff;

    cudaMalloc((void**)&d_in , memsize);
    cudaMalloc((void**)&d_out, memsize);
    cudaMalloc((void**)&d1_out, memsize);

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < collums; j++){
            m_in[i*collums+j] = 1.0+j;
        }
    }
    cudaMemcpy(d_in, m_in, memsize, cudaMemcpyHostToDevice);
    matrix_operation(m_in,seq_m_out,rows);


    gettimeofday(&t_start, NULL);
    if(transposed){/*
        dim3 block1(T,T,1), grid1(2,dimy,1);
        tilling_transpose_kernel<T><<<grid1,block1>>>(d_in,d_out,rows,collums);
        cudaThreadSynchronize();
        matrix_operation_optimized_kernel<<<grid,block>>>(d_out,d1_out,rows);
        cudaThreadSynchronize();
        dim3 block1(T,T,1), grid1(dimy,2,1);
        tilling_transpose_kernel<T><<<grid1,block1>>>*/
    }else{
        matrix_operation_naive_kernel<<<grid,block>>>(d_in,d_out,rows);
        cudaThreadSynchronize();
    }
    gettimeofday(&t_end, NULL);
    cudaMemcpy(m_out, d_out, memsize, cudaMemcpyDeviceToHost);

    bool valid = true;
    for(int i = 0; i < rows; i++){
        for(int j=0; j < collums; j++){
            float diff = abs(m_out[i*collums+j]-seq_m_out[i*collums+j]);
            if(diff > 0.00001){
                valid = false;
            }
        }
    }
    if(valid){
        std::cout << "Valid result\n";
    }else{
        std::cout << "Invalid result\n";
    }
    timeval_subtract(&t_diff,&t_end,&t_start);
    elapsed=(t_diff.tv_sec*1e6 +t_diff.tv_usec);
    double flops = rows*collums;
    double gigaFlops=(flops*1.0e-3f) / elapsed;
    printf("Amount of GFPLOS %f \n",gigaFlops);
    
    free(seq_m_out);
    free(m_out);
    free(m_in);
    cudaFree(d_in);
    cudaFree(d_out);


}

void multiply_test(bool tilling){

    unsigned long int elapsed;
    struct timeval t_start,t_end,t_diff;
    int const T = 32;
    int rows = 258;
    int m1_collums = 272;
    int m2_collums = 288;
    int dimx = (m2_collums+T-1)/T;
    int dimy = (rows+T-1)/T;
    dim3 block(T,T,1), grid(dimx,dimy,1);
    int mem_i1 = sizeof(float)*rows*m1_collums;
    int mem_i2 = sizeof(float)*m2_collums*m1_collums;
    int mem_o = sizeof(float)*rows*m2_collums;
    float *m1_in = (float*) malloc(mem_i1);
    float *m2_in = (float*) malloc(mem_i2);
    float *m_out = (float*) malloc(mem_o);
    float *seq_m_out = (float*) malloc(mem_o);
    for(int i = 0; i < rows; i++){
        for(int j = 0; j < m1_collums; j++){
            m1_in[i*m1_collums+j] = 1.0+j;
        }
    }
    for(int i = 0; i < m1_collums; i++){
        for(int j = 0; j < m2_collums; j++){
            m2_in[i*m2_collums+j] = 1.0+j;
        }
    }

    float *d1_in, *d2_in, *d_out;
    cudaMalloc((void**)&d1_in , mem_i1);
    cudaMalloc((void**)&d2_in , mem_i2);
    cudaMalloc((void**)&d_out, mem_o);


    cudaMemcpy(d1_in, m1_in, mem_i1, cudaMemcpyHostToDevice);
    cudaMemcpy(d2_in, m2_in, mem_i2, cudaMemcpyHostToDevice);

    gettimeofday(&t_start, NULL);
    if(tilling){
        tilling_mult_kernel<T><<<grid,block>>>(d1_in,d2_in,d_out,rows,m1_collums,m2_collums);
        cudaThreadSynchronize();
    }else{
        naive_mult_kernel<<<grid,block>>>(d1_in,d2_in,d_out,rows,m1_collums,m2_collums);
        cudaThreadSynchronize();
    }
    gettimeofday(&t_end, NULL);

    cudaMemcpy(m_out, d_out, mem_o, cudaMemcpyDeviceToHost);
    seq_matrixmul(m1_in,m2_in,seq_m_out,rows,m1_collums,m2_collums);
    bool valid = true;
    for(int i = 0; i < rows; i++){
        for(int j = 0; j < m2_collums; j++){
            if(seq_m_out[i*m2_collums+j] != m_out[i*m2_collums+j]){
                valid = false;
            }
        }
    }
    if(valid){
        std::cout << "Matrix mult valid\n";
    }else{
        std::cout << "Matrix mult invalid\n";
    }
    timeval_subtract(&t_diff,&t_end,&t_start);
    elapsed=(t_diff.tv_sec*1e6 +t_diff.tv_usec);
    double flops = rows*m2_collums*m1_collums;
    double gigaFlops=(flops*1.0e-3f) / elapsed;
    printf("Amount of GFPLOS %f \n",gigaFlops);
    cudaFree(d2_in);
    cudaFree(d1_in);
    cudaFree(d_out);
    free(m1_in);
    free(m2_in);
    free(m_out);
    free(seq_m_out);
}

int main(int argc, char** argv) {
    std::cout << "Transpose tests without and with tilling\n";
    transpose_test(false);
    transpose_test(true);
    std::cout << "assignment 2 matrix op test\n";
    assign2_test(false);
    std::cout << "multiply tests without and with tilling\n";
    multiply_test(false);
    multiply_test(true);
    return 0;
}


