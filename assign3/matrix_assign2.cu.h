#import <math.h>

/*
* Sequential implementation of task 2 takes flat float array m_in and m_out of size [n,64]
*/
void matrix_operation(float *m_in, float *m_out, int n){
    for(int i = 0; i < n; i++){
        float accum = m_in[i*64]*m_in[i*64];
        m_out[i*64] = accum;
        for(int j = 1; j < 64; j++){
            float tmp_a = m_in[i*64+j];
            accum = sqrt(accum) + tmp_a*tmp_a;
            m_out[i*64+j] = accum;
        }
    }
}


__global__ void matrix_operation_naive_kernel(float *m_in, float *m_out, int n){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if(i >= n){ 
        return;
    }
    float accum = m_in[i*64]*m_in[i*64];
    m_out[i*64] = accum;
    for(int j = 1; j < 64; j++){
        float tmp_a = m_in[i*64+j];
        accum = sqrt(accum)+tmp_a*tmp_a;
        m_out[i*64+j] = accum;
    }
}

__global__ void matrix_operation_optimized_kernel(float* m_in, float* m_out, int rows){
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    if(j >= rows){
        return;
    }
    float accum = m_in[rows*j]*m_in[rows*j];
    m_out[rows*j] = accum;
    for(int i = 1; i < 64; i++){
        float tmp_a = m_in[rows*j+i];
    }
}