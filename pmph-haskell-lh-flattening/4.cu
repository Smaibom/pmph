#include <stdio.h>
#include <string.h>
#include <math.h>
#include <cuda_runtime.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

__global__ void squareKernel(float* d_in, float *d_out, int num_threads) {
	const unsigned int lid = threadIdx.x; // local id inside a block
	const unsigned int gid = blockIdx.x*blockDim.x + lid; // global id
	if(gid < num_threads){
		d_out[gid] = powf((d_in[gid]/(d_in[gid]-2.3)),3);
	}	
}

int timeval_subtract(struct timeval* result,
					 struct timeval* t2,struct timeval* t1) {
	unsigned int resolution=1000000;
	long int diff = (t2->tv_usec + resolution * t2->tv_sec) -
					(t1->tv_usec + resolution * t1->tv_sec) ;
	result->tv_sec = diff / resolution;
	result->tv_usec = diff % resolution;
	return (diff<0);
}

int main(int argc, char** argv) {
	unsigned int num_threads = 753411;
	unsigned int mem_size = num_threads*sizeof(float);
	unsigned int block_size = 256;
	unsigned int num_blocks = ((num_threads+ (block_size-1)) / block_size);
	unsigned long int elapsed_par, elapsed_serial;
	struct timeval t_start, t_end, t_diff;

	// allocate host memory
	float* h_in = (float*) malloc(mem_size);
	float* h_out = (float*) malloc(mem_size);
	float h_serial_out[num_threads];

	// initialize the memory
	for(unsigned int i=0; i<num_threads; ++i){
		h_in[i] = (float)i;
	}

	gettimeofday(&t_start, NULL);
	for(int i=0; i<num_threads; ++i){
		h_serial_out[i] = powf((h_in[i]/(h_in[i]-2.3)),3.0);
	}
	gettimeofday(&t_end, NULL);
	timeval_subtract(&t_diff, &t_end, &t_start);
	elapsed_serial = t_diff.tv_sec*1e6+t_diff.tv_usec;
	printf("Serial computation took %d microseconds (%.2fms)\n",elapsed_serial,elapsed_serial/1000.0);
	// allocate device memory
	float* d_in;
	float* d_out;
	cudaMalloc((void**)&d_in, mem_size);
	cudaMalloc((void**)&d_out, mem_size);
	// copy host memory to device
	cudaMemcpy(d_in, h_in, mem_size, cudaMemcpyHostToDevice);

	// execute the kernel
	gettimeofday(&t_start, NULL);
	squareKernel<<< num_blocks, block_size>>>(d_in, d_out,num_threads);
	cudaThreadSynchronize();
	gettimeofday(&t_end, NULL);
	
	timeval_subtract(&t_diff, &t_end, &t_start);
	elapsed_par = t_diff.tv_sec*1e6+t_diff.tv_usec;

	printf("Parallel computation took %d microseconds (%.2fms)\n",elapsed_par,elapsed_par/1000.0);
	// copy result from ddevice to host
	cudaMemcpy(h_out, d_out, sizeof(float)*num_threads, cudaMemcpyDeviceToHost);


	float epsilon = 1e-3;
	int correct = 1;

	// Compare with an epsilon value
	for(unsigned int i=0; i<num_threads; ++i) {
		float diff = abs(h_serial_out[i]-h_out[i]);
		if(diff > epsilon){
			correct = 0;
		}
	}

	if(correct){
		printf("All results are the same\n");
	}
	else{
		printf("Mismatches in results\n");
	}

	// clean-up memory
	free(h_in);
	free(h_out);
	cudaFree(d_in);
	cudaFree(d_out);
}