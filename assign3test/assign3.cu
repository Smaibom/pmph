#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <time.h>
#include <sys/time.h>
#include "transpose.cu.h"
#include "matrix_mul.cu"

int timeval_subtract(struct timeval* result,
                     struct timeval* t2,struct timeval* t1) {
    unsigned int resolution=1000000;
    long int diff = (t2->tv_usec + resolution * t2->tv_sec) -
                    (t1->tv_usec + resolution * t1->tv_sec) ;
    result->tv_sec = diff / resolution;
    result->tv_usec = diff % resolution;
    return (diff<0);
}


void transpose_test(bool tilling = false){
    int rows = 10000;
    int collums = 12000;
    int grid_y = 16;
    int grid_x = 16;
    int block_x_size = (collums+grid_x-1)/grid_x;
    int block_y_size = (rows+grid_y-1)/grid_y;
    dim3 grid_size(grid_x,grid_y), block_size(block_x_size,block_y_size);

    int memsize = sizeof(float)*rows*collums;
    float *m_in = (float*) malloc(memsize);
    float *serial_out = (float*) malloc(memsize);
    float *d_in;
    float *d_out;

    unsigned long int elapsed;
    struct timeval t_start,t_end,t_diff;

    cudaMalloc((void**)&d_in , memsize);
    cudaMalloc((void**)&d_out, memsize);

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < collums; j++){
            m_in[i*collums+j] = 1.0+j;
        }
    }


    cudaMemcpy(d_in, m_in, memsize, cudaMemcpyHostToDevice);

    gettimeofday(&t_start, NULL);
    if(tilling){
        const int T = 32;
        tilling_transpose_kernel<T><<<block_size,grid_size>>>(d_in,d_out,rows,collums);
        cudaThreadSynchronize();
    } else {
        naive_transpose_kernel<<<block_size,grid_size>>>(d_in,d_out,rows,collums);
        cudaThreadSynchronize();
    }
    gettimeofday(&t_end, NULL);
    cudaMemcpy(serial_out, d_out, memsize, cudaMemcpyDeviceToHost);

    bool valid = true;
    for(int i = 0; i < rows; i++){
        for(int j = 0; j < collums; j++){
            if(serial_out[j*rows+i] != m_in[i*collums+j]){
                valid = false;
            }
        }
    }
    if(valid){
        std::cout << "Transpose valid \n";
    }else{
        std::cout << "Transpose invalid \n";
    }

    timeval_subtract(&t_diff,&t_end,&t_start);
    elapsed=(t_diff.tv_sec*1e6 +t_diff.tv_usec);
    double flops = rows*collums;
    double gigaFlops=(flops*1.0e-3f) / elapsed;
    printf("Amount of GFPLOS %f \n",gigaFlops);
}

int main(int argc, char** argv) {

    transpose_test(false);
    transpose_test(true);
    return 0;
}


