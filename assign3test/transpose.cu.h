
/*
* 1.a Sequential implementation of transpose
* Takes a flatten matrix_in of dimension [row,collum] and transposes it to flatten matrix_out of dimension [collum,row]
*/
void seq_transpose(float *matrix_in,float *matrix_out,int row, int collum){
    for(int i = 0; i < row; i++){
        for(int j = 0; j < collum; j++){
            matrix_out[j*row+i] = matrix_in[i*collum+j];
        }
    }
}

/*
* 1.c Naive implementation of transpose, takes a flat array m_in and m_out of size m*n,
* and the number of rows and collums in the input matrix
*/
__global__ void naive_transpose_kernel(float *m_in, float *m_out, int num_rows ,int num_collums) {
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    if(i >= num_rows || j >= num_collums) {
        return;
    }
    m_out[j*num_rows+i] = m_in[i*num_collums+j];
}

/*
* 1.d cuda optimized transpose
*/

template <int T>
__global__ void tilling_transpose_kernel(float *m_in, float *m_out, int num_rows ,int num_collums) {
    __shared__ float tile[T][T+1];
    int i = threadIdx.y;
    int j = threadIdx.x;
    int ii = blockIdx.y*T+i;
    int jj = blockIdx.x*T+j;
    if(ii < num_rows && jj < num_collums) {
        tile[i][j] = m_in[ii*num_collums+jj];
    } 
    __syncthreads();
    ii = blockIdx.y*T+j;
    jj = blockIdx.x*T+i;
    if( jj < num_collums && ii < num_rows){
        m_out[jj*num_rows+ii] = tile[j][i];
    }

}