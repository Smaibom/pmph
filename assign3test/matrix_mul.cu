/* Multiplies 2 matrixes m1_in and m2_in with dimensions of [m1_rows,m1_collums] 
* and [m1_collums,m2_collums] and places the result in m_out of dimensions [m1_rows,m2_collums]
* it is presumed that input matrixes uphold the dimensions of n x p and p x m 
* and the output has dimensions n x m, otherwise the function fails
*/

void seq_matrixmul(float *m1_in,float *m2_in, float *m_out,int m1_rows, int m1_collums, int m2_collums){
    for(int i = 0; i < m1_rows; i++){
        for(int j = 0; j < m2_collums; j++){
            float tmp = 0.0;
            for(int k = 0; k < m1_collums; k++){
                tmp += m1_in[i*m1_rows+k]*m2_in[k*m1_collums+j];
            }
            m_out[i*m1_rows+j] = tmp;
        }
    }
}

/*
* 3.c Naive implementation of matrix multiply, takes a flat array m_in and m_out of size m*n,
* and the number of rows and collums in the input matrix
*/
__global__ void naive_mult_kernel(float *m1_in, float* m2_in, float *m_out, int m1_rows ,int m1_collums,int m2_collums) {
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    if(i >= m1_rows || j >= m2_collums) {
        return;
    }
    float tmp = 0.0;
    for(int k = 0; k < m1_collums; k++){
        tmp += m1_in[i*m1_rows+k]*m2_in[k*m1_collums+j];  
    }
    m_out[j*m1_rows+i] = tmp;
}

/*
* Not finished tilling multiply of matrixes
*/
template<int T>
__global__ void tilling_mult_kernel(float *m1_in, float* m2_in, float *m_out, int m1_rows ,int m1_collums,int m2_collums) {
    __shared__ float tile_1[T][T], tile2_[T][T];
    int tidy = threadIdx.y;
    int tidx = threadIdx.x;
    int ii = blockIdx.y*T+tidy;
    int jj = blockIdx.x*T+tidx;
    int j = tidx+jj, i = tidy+ii;
    /*for(int kk=0; kk<m1_collums; kk+=T) {
        tile_1[i][j] = (i< && kk+tidx<U) ? m1_in[i*m1_rows+kk+j] : 0.0; 
        tile_2[i][j] = (j<N && kk_tidy<U) ? m2_in[kk+tidy] : 0.0;
    } 
    __syncthreads();*/
}